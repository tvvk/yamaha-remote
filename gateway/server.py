import os, web, requests

urls = (
  '/powerOn', 'powerOn',
  '/powerOff', 'powerOff',
  '/input/tuner', 'inputTuner',
  '/input/digital1', 'inputDigital1',
)

class powerOn:
  def GET(self):
    return send("powerOn", "<Power_Control><Power>On</Power></Power_Control>")
class powerOff:
  def GET(self):
    return send("powerOff", "<Power_Control><Power>Standby</Power></Power_Control>")
class inputTuner:
  def GET(self):
    return send("tuner", "<Input><Input_Sel>TUNER</Input_Sel></Input>")
class inputDigital1:
  def GET(self):
    return send("digital1", "<Input><Input_Sel>DIGITAL1</Input_Sel></Input>")

def send(name, payload):
  host = os.environ["YAMAHA_GW_HOST"]
  secret = os.environ["YAMAHA_GW_SECRET"]

  if (secret != web.input().secret):
    print("Wrong secret given")
    return "Ok"

  theData = '<YAMAHA_AV cmd="PUT"><System>' + payload + '</System></YAMAHA_AV>';
  print("Sending request", name, theData)
  r = requests.post("http://" + host + "/YamahaRemoteControl/ctrl", data=theData)
  print(r.status_code, r.reason)
  return r.reason

if __name__ == "__main__":
  app = web.application(urls, globals())
  app.run()




